.. title: About Me
.. date: 2021-04-30 00:00:00

Hi there 👋 I am Zi He, welcome to my humble about page. If you wish to contact me or just see my works, you can find my socials below. Otherwise, you can find most
of my code stuff on my `GitHub`_.

Here's a trip down memory lane:
 - Pre-2009
    - Interested in making video games since young, I still recall organizing some game design notes when I was around 14 years old.
    - Interested in playing video games as well, only had a few video games that I always played and enjoy.
    - Remembered making a "quiz"-like game in Microsoft Powerpoint.
    - Probably somewhere around late 2000s, I picked up a book introducing Knoppix, a LiveCD that is able to boot Linux. My first foray into Linux.
 - 2009 - 2012
    - Learned more about making games (mainly Flash, Unity) from Singapore Polytechnic.
    - Learned several frameworks like FlashPunk, Flixel and used (mainly FlashPunk) to make video games for my school projects.
    - Created several Flash games, most of which I probably cannot show now due to Flash being deprecated and most of them being part of school or internship.
    - First attempt at learning Python and Panda3D however I was not as serious at learning it at that point of time.
    - Picked up Unity's Javascript and C# for making video games.
 - 2012 - 2014
    - Learned about `Haxe`_ cross-platform toolkit and it's ecosystems. Haxe is a great tool for people coming from Flash ecosystem.
    - Tried to port a couple of my old games from FlashPunk (Flash) to HaxePunk (Haxe).
    - Also tried to developed my own `2D framework`_ sort of thing in Haxe with OpenFL (another Haxe flash like framework)
    - Developed my own mini prototypes based on the 2D framework I developed, did not become a full game.
    - Memory is hazy on this, but I remembered joining a couple of (DBS) game jams and using Flash/Haxe/Unity.
 - 2014 - 2018
    - Decided to study CS in DigiPen. Made a couple of games in C++, those were exciting times. We have a team that build the whole game engine and tools needed to run our game.
    - Learned C, C++ (2011) in school and did all of our projects in C++.
    - Did some OpenGL assignments to draw polygons on screen.
    - Discovered Imgui library and pioneered the adoption of Imgui at my game project class by introducing tool and proof of concept in class.
    - Made 5 games in the span of 4 years in school, 3 games were with custom C++/OpenGL/DirectX engine, 1 with a OpenGL framework, 1 with a game engine provided by the school.
    - Unofficial achievement that I count is being considered one of the most engaging game that our team made in our sophomore year. We got the praise of teachers and peers alike.
    - Probably did 1/2 game jams (Unity) here as well with my University classmates.
    - Learned about web back-end developement (Flask) and front-end (angularjs) while working to build a web tool at my first internship.
    - Experimented with a couple of static site generators, I think my first ever site was generated from Jekyll, the most popular website generator back then.
    - Finally settled into this current site with the help of Nikola static site generator. Ever since, I have updated with different looks and feels for the page.
 - 2018 - 2019
    - Working on desktop application components written in C++ as an intern. Learned more about the software development cycle and Agile.
    - Got into a bank on a technology associate program. Learned about the different process and also first time working in a web application back-end role.
    - Got some working experience in building and testing applications in Linux machines, using ssh, tmux, bash, nano, vi etc.
    - Code in my workplace are mostly written in Java. Personally not a big fan of Java because of it's verbose, however I do feel there is always a demand for Java developers.
    - Somewhere around this time, went around to picking up a new programming language, D. I was looking for something similar to C++ without the complexity of it.
    - Wrote an FTP, Chip-8 and a Space Invader game in D as a learning exercise.
    - Attempt (my first) Advent of code up till day 6 in D.
 - 2020 - 2021
    - Got a new job and worked on various other stuff here from work.
    - Sometime in 2020, the website 3D Buzz announced that it is closing down and I decided to write a script to parse and download all the contents of the page.
    - Interest in Vim editors brought me to using Neovim. That is one of my favorite editors I installed on almost all of my work/personal machines.
    - Somewhere around this time, I got to (re)learning Java and Python so that I can tutor students in my free time.
    - Wrote a python script for one of the Shopee events to guess the correct sequence of number to redeem their voucher.
    - Interest in open source operating systems brought me to try out FreeBSD for the first time. I managed to install it and run on my personal laptops.
    - Also tried my hands on some new programming language like Julia and F#. Those are really nice language to work with.
    - Got interested in python and visualization stuff at some point and built my own analytics dashboard with Python Dash library.
    - Also attempted to install various other \*BSD like NetBSD and OpenBSD to try, ultimately still stick with FreeBSD.
    - Also been interested in Emacs as well, being trying to learn it on an off. Decided to give emacs a try by using Doom Emacs, (credits to distrotube).
    - I think it was around here that I got curious about desktop tiling manager (dwm, i3, and the likes), got to try and love herbstluftwm, also tried my hands on xmonad.
 - 2022 - Present
    - Attempt at solving Advent of code 2021 in Python

I guess that's it folks!


.. _GitLab: https://gitlab.com/zhgoh
.. _GitHub: https://github.com/zhgoh
.. _2d framework: https://gitlab.com/zhgoh/ze2d
.. _Haxe: https://haxe.org/
