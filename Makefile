all:
	echo "make run"
	echo "make post"
	echo "make github"

run:
	cd blog && nikola auto

post:
	cd blog && nikola new_post

github:
	push_github.bat
