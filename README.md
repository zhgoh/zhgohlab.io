My Blog
=======

This will be my blog where I document my projects and share my thoughts.

Tools
-----

Currently this blog is generated using [Nikola](https://getnikola.com/).

Note
----

This blog is on my GitLab at the moment because of how GitLab builds the page, it requires the whole website project and uses CI to build a page while GitHub just needs the final html to show the page. 

Therefore, I am putting my blog on GitLab which uses the CI to build the page and hosting the output on GitHub as well.

To push to GitHub, just use the batch file provided.